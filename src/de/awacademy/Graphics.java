package de.awacademy;

import javafx.scene.image.Image;
import de.awacademy.model.Model;
import javafx.scene.canvas.GraphicsContext;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Graphics {

    private GraphicsContext gc;
    private Model model;


    public Graphics(GraphicsContext gc, Model model) {
        this.gc = gc;
        this.model = model;
    }


    public void draw() throws FileNotFoundException {

            gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);

            Image img = new Image(new FileInputStream("src/de/awacademy/Images/Fußballtor-Kauf-beim-Hersteller.jpg"));
            gc.drawImage(img, 0, 0, model.WIDTH, model.HEIGHT);
            Image img1 = new Image(new FileInputStream("src/de/awacademy/Images/kp.png"));
            Image img2 = new Image(new FileInputStream("src/de/awacademy/Images/ball.png"));

        Image img5 = new Image(new FileInputStream("src/de/awacademy/Images/m1.jpg"));
            gc.drawImage(img5, 50, -50);


            gc.drawImage(img2, model.getBall().getX(), model.getBall().getY(),
                    model.getBall().getW(), model.getBall().getH());

            gc.drawImage(img1, model.getTorwart().getX(), model.getTorwart().getY(),
                    model.getTorwart().getW(), model.getTorwart().getH()

            );

        if (!model.isCollosionCheck() && model.getBall().getX() != 450&&model.getBall().getY()!=500) {
            Image img3 = new Image(new FileInputStream("src/de/awacademy/Images/gol.png"));

            gc.drawImage(img3, 0, 50);

        }
            if (model.isCollosionCheck()&&model.getBall().getX()!=450&&model.getBall().getY()!=500) {
                Image img4 = new Image(new FileInputStream("src/de/awacademy/Images/ngol.png"));
                gc.drawImage(img4, 0, 0);


            }


        }

}