package de.awacademy.model;


public class Player {

    private int x;
    private int y;
    private int h;
    private int w;


    public Player(int x, int y,int h, int w) {
        this.x = x;
        this.y = y;
        this.h = h;
        this.w = w;
    }

    public  void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }


    public void stop(int dx,int dy){
        this.x=dx;
        this.y=dy;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }


}
