package de.awacademy.model;

public class Model   {

    private Player ball;
    private Player torwart;
    private boolean collosionCheck;



    public boolean isCollosionCheck() {
        return collosionCheck;
    }


    public final double WIDTH = 1000;
    public final double HEIGHT = 600;

    public Model() {
        this.ball = new Player(450, 500, 90,90);
        this.torwart = new Player(450,250,180,150);

    }

    public Player getBall() {
        return ball;
    }

    public Player getTorwart() {
        return torwart;
    }

    public void update(long nowNano) {

            if (Math.abs(getBall().getX() - getTorwart().getX()) < 100 &&
                    Math.abs(getBall().getY() - getTorwart().getY()) < 200) {
                collosionCheck = true;

            }else{
                collosionCheck=false;
            }

            if (getBall().getX() == 450 && getBall().getY() == 500) {
                torwart.move(10, 0);
            }else {
                torwart.move(0,0);
            }
            if (getTorwart().getX() >= 850) {
                torwart.move(-700, 0);

            }

        }

}
