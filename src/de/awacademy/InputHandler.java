package de.awacademy;

import de.awacademy.model.Model;
import javafx.scene.input.KeyCode;

public class InputHandler {

    private Model model;
    private boolean multiPlayer;


    public boolean isMultiPlayer() {
        return multiPlayer;
    }

    public InputHandler(Model model) {
        this.model = model;
    }


    public void onKeyPressed(KeyCode keycode) {

            if (keycode == KeyCode.NUMPAD1) {
                model.getBall().move(-320, -180);
                model.getBall().stop(130,320);
            }

            if (keycode == KeyCode.NUMPAD2) {
                model.getBall().move(50, -170);
                model.getBall().stop(500,330);

            }

            if (keycode == KeyCode.NUMPAD3) {
                model.getBall().move(350, -160);
                model.getBall().stop(800,340);

            }

            if (keycode == KeyCode.NUMPAD4) {
                model.getBall().move(-300, -300);
                model.getBall().stop(150,200);

            }

            if (keycode == KeyCode.NUMPAD5) {
                model.getBall().move(-50, -200);
                model.getBall().stop(400,300);

            }

            if (keycode == KeyCode.NUMPAD6) {
                model.getBall().move(350, -250);
                model.getBall().stop(100,250);

            }

            if (keycode == KeyCode.NUMPAD7) {
                model.getBall().move(-370, -350);
                model.getBall().stop(80,150);

            }

            if (keycode == KeyCode.NUMPAD8) {
                model.getBall().move(-30, -350);
                model.getBall().stop(420,180);

            }

            if (keycode == KeyCode.NUMPAD9) {
                model.getBall().move(350, -360);
                model.getBall().stop(800,140);

            }

            if (keycode == KeyCode.NUMPAD0) {
                model.getBall().move(-50, -350);
                model.getBall().stop(400,150);

            }

            if(keycode==KeyCode.ENTER) { // get the ball back!
                 if (model.getBall().getX() <= 500) {

                    model.getBall().move((Math.abs(model.getBall().getX() - 450)), (Math.abs(model.getBall().getY() - 500)));
                }

                if(model.getBall().getX()>500){

                    model.getBall().move(((450-model.getBall().getX())), (500-model.getBall().getY()));

                }
            }

            if (keycode == KeyCode.M) {
                multiPlayer = true;

            }
            if (isMultiPlayer()) {
                if (keycode == KeyCode.RIGHT) {
                    model.getTorwart().move(180, 0);
                }

                if (keycode == KeyCode.LEFT) {
                    model.getTorwart().move(-180, 0);
                }


            }
        }
    }
